/* global L, distance */
var mapa;

var geojson;

const LAT = 46.047765;
const LNG = 14.509809;

var latlng;

window.addEventListener('load', function () {

  var mapOptions = {
    center: [LAT, LNG],
    zoom: 12
  };

  // Ustvarimo objekt mapa
  mapa = new L.map('mapa_id', mapOptions);

  // Ustvarimo prikazni sloj mape
  var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

  // Prikazni sloj dodamo na mapo
  mapa.addLayer(layer);

  function obKlikuNaMapo(e) {
    latlng = e.latlng;
    //izrisiBolnice();
  }

  mapa.on('click', obKlikuNaMapo);
  
  izrisiBolnice();
});

function izrisiBolnice() {

  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open("GET", "https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json", true);
  xobj.onreadystatechange = function () {
    // rezultat ob uspešno prebrani datoteki
    if (xobj.readyState == 4 && xobj.status == "200") {
        var json = JSON.parse(xobj.responseText);

       geojson =L.geoJSON(json.features, {
          style: {color: 'blue'},
          onEachFeature: function (feature, layer) {
             layer.bindPopup('<b>'+feature.properties.name+'</b><br>'+ feature.properties["addr:street"] +' '+ feature.properties["addr:housenumber"]+', '+feature.properties["addr:city"]);
          },
          filter: function (feature) {
              if (feature.geometry.type != "Point") return true;
            }
        });
        geojson.addTo(mapa);
      }
    };
   xobj.send(null);
}