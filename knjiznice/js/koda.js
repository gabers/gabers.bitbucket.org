/*global d3*/
/* global $ */
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";



window.addEventListener('load', function () {
  graf();
  document.getElementById("generirajPodatke").addEventListener('click', generirajPodatke);
  document.getElementById("vnosGumb").addEventListener('click',vnosUporabnika);
  document.getElementById("izborGumb").addEventListener('click', izbraniPacient);
  document.getElementById("ehrGumb").addEventListener('click', ehrPridobi);
});

/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}

var ime;
var priimek;
var ehr;
var visina=1;
var teza=1;
var tlakS=0;
var tlakD=0;
var bmi;
var pripravljenost=0;


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  var ehrId = "";

  // TODO: Potrebno implementirati
  
  if(stPacienta==1){
    ime="Franc";
    priimek="Lačni";
    visina="185";
    teza="58";
    tlakS="120";
    tlakD="58";
  }
  else if(stPacienta==2){
    ime="Martin";
    priimek="Krpan";
    visina="192";
    teza="70";
    tlakS="120";
    tlakD="75";
  }
  else if(stPacienta==3){
    ime="Jože";
    priimek="Običajni";
    visina="165";
    teza="72";
    tlakS="135";
    tlakD="92";
  }
  $.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          additionalInfo: {"ehrId": ehrId}
        };
        var podatki = {
  			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
        // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
  		    "ctx/language": "en",
          "ctx/territory": "SI",
  		    "vital_signs/height_length/any_event/body_height_length": visina,
  		    "vital_signs/body_weight/any_event/body_weight": teza,
  		    "vital_signs/blood_pressure/any_event/systolic": tlakS,
  		    "vital_signs/blood_pressure/any_event/diastolic": tlakD
        };  
        var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT'
};
        $.ajax({
      url: baseUrl + "/composition?" + $.param(parametriZahteve),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(podatki),
      headers: {
        "Authorization": getAuthorization()
},       success: function (res) {
        
      },
      error: function(err) {
      	$(JSON.parse(err.responseText).userMessage);
      }
});
        console.log(ehrId);
        ehr=ehrId;
      }
  });
  return ehrId;
}

/*function ehrPridobi() {
  var ehrId = document.getElementById("generirajPodatke").value;
  $.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {
          "Authorization": getAuthorization()
        },
	    	success: function (data) {
  				var party = data.party;
  				$("#rezultatMeritveVitalnihZnakov").html("<br/><span>Pridobivanje " +
            "podatkov za <b>'" + tip + "'</b> bolnika <b>'" + party.firstNames +
            " " + party.lastNames + "'</b>.</span><br/><br/>");
  				if (tip == "telesna temperatura") {
  					$.ajax({
    				  url: baseUrl + "/view/" + ehrId + "/" + "body_temperature",
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) {
    			    	if (res.length > 0) {
    				    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna temperatura</th></tr>";
  				        for (var i in res) {
    		            results += "<tr><td>" + res[i].time +
                      "</td><td class='text-right'>" + res[i].temperature +
                      " " + res[i].unit + "</td></tr>";
  				        }
  				        results += "</table>";
  				        $("#rezultatMeritveVitalnihZnakov").append(results);
    			    	} else {
    			    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
    			    	}
    			    },
    			    error: function() {
    			    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
    			    }
  					});
  				} else if (tip == "telesna teža") {
  					$.ajax({
    			    url: baseUrl + "/view/" + ehrId + "/" + "weight",
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) {
    			    	if (res.length > 0) {
    				    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna teža</th></tr>";
  				        for (var i in res) {
    		            results += "<tr><td>" + res[i].time +
                      "</td><td class='text-right'>" + res[i].weight + " " 	+
                      res[i].unit + "</td></tr>";
  				        }
  				        results += "</table>";
  				        $("#rezultatMeritveVitalnihZnakov").append(results);
    			    	} else {
    			    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
    			    	}
    			    },
    			    error: function() {
    			    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
    			    }
  					});
  				}
	    	},
	    	error: function(err) {
	    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});
}
});
}
*/
function izbraniPacient(){
  var stPacienta=document.getElementById("izbiraPacienta").value;
  if(stPacienta==1){
    ime="Franc";
    priimek="Lačni";
    visina="185";
    teza="58";
    tlakS="120";
    tlakD="58";
  }
  else if(stPacienta==2){
    ime="Martin";
    priimek="Krpan";
    visina="192";
    teza="70";
    tlakS="120";
    tlakD="75";
  }
  else if(stPacienta==3){
    ime="Jože";
    priimek="Običajni";
    visina="165";
    teza="72";
    tlakS="135";
    tlakD="92";
  }
  izpis();
}

function vnosUporabnika(){
  ime=document.getElementById("vnosIme").value;
  priimek=document.getElementById("vnosPriimek").value;
//  ehr=document.getElementById("vnosEhr").value;
  visina=document.getElementById("vnosVisina").value;
  teza=document.getElementById("vnosTeza").value;
  tlakS=document.getElementById("vnosTlakS").value;
  tlakD=document.getElementById("vnosTlakD").value;
  izpis();
}

function izpis(){
   document.getElementById("izpisIme").textContent=' '+ime;
   document.getElementById("izpisPriimek").textContent=' ' + priimek;
 //  document.getElementById("izpisEhr").textContent=' ' +ehr;
   document.getElementById("izpisVisina").textContent=' ' +visina;
   document.getElementById("izpisTeza").textContent=' ' +teza;
   document.getElementById("izpisTlakS").textContent=' ' +tlakS;
   document.getElementById("izpisTlakD").textContent=' ' +tlakD;
   
   bmi=teza/((visina/100)**2);
    
    if(bmi>=18.5 && bmi<25){
      pripravljenost+=3;
    }
    else if((bmi>=17&& bmi<18.5)||(bmi>25 && bmi<30)){
      pripravljenost++;
    }
    
    if(tlakD>=60 && tlakD<90){
      pripravljenost++;
    }
    
    if(tlakS>=110 && tlakS<140){
      pripravljenost++;
    }
    
    graf();
    d3.select("svg").remove();   
    wikiA();
    wikiB();
    pripravljenost=0;


}

function graf(){
  var $container = d3.select('#linear-gauge');
  var width = parseFloat($container.style("width"));
  var height = parseFloat($container.style("height"));
  
  window.onresize = resize;
  
  function resize()
  {
    $container = d3.select('#linear-gauge');
    width = parseFloat($container.style("width"));
    height = parseFloat($container.style("height"));
  
   
  	chart_w = width;
  	resultPos = chart_w * result;
  
  	text_margins = {top: chart_y_pos + gauge_h + 35, right: 10, bottom: 0, left: 10};
  
    
    d3.select("line")
    .attr("x1", resultPos)
  	.attr("y1", chart_y_pos )
    .attr("x2", resultPos )
  	.attr("y2", gauge_h + chart_y_pos );
  
    d3.select("circle")
    .attr("cx", resultPos)
    .attr("cy", (gauge_h + chart_y_pos) / 2 );
  
    d3.select(".rightLabel")
    .attr("x", chart_w )
    .text( "width: " + width );
  
    d3.select(".rightPrcnt")
    .attr("x", chart_w );
   
  }
  
  
  // Tick mark
  
  var LF = 30;
  
  var gauge_h = 60;
  
  var chart_w = width;
  var chart_y_pos = 0;
  
  var result = pripravljenost/5;	// in a scale [0 1]
  var resultPos = chart_w * result;
  
  var text_margins = {top: chart_y_pos + gauge_h + 35, right: 10, bottom: 0, left: 10};
  
  // Chart size -----------
  
  var svg = d3.select('#linear-gauge').append("svg")
  .attr("width", '100%')
  .attr("height", '100%');
  
  var gradient = svg.append("svg:defs")
    .append("svg:linearGradient")
      .attr("id", "gradient")
      .attr("x1", "0%")
      .attr("y1", "0%")
      .attr("x2", "100%")
      .attr("y2", "0%")
      .attr("spreadMethod", "pad");
  
  gradient.append("svg:stop")
      .attr("offset", "0%")
      .attr("stop-color", "#c00")
      .attr("stop-opacity", 1);
  
  gradient.append("svg:stop")
      .attr("offset", "50%")
      .attr("stop-color", "yellow")
      .attr("stop-opacity", 1);
  
  
  gradient.append("svg:stop")
      .attr("offset", "100%")
      .attr("stop-color", "#0c0")
      .attr("stop-opacity", 1);
  
  svg.append("g")
  	.append("rect")
    .attr("x", 0 )
    .attr("y", chart_y_pos )
    .attr("width", "100%" )
    .attr("height", gauge_h )
    .style("fill", "url(#gradient)");
  
  
  /****************************************
  * Text, titles
  *****************************************/
  
  // Left percentage indicator
  svg.append("g")
  	.append("text")
    .attr("x", 0)
    .attr("y", text_margins.top )
    .text( "Slaba pripravljenost" );
  
  // Right percentage indicator
  
  svg.append("g")
  	.append("text")
  	.classed("rightPrcnt", true )
    .attr("x", chart_w )
    .attr("y", text_margins.top )
  	.attr("text-anchor", "end")
    .text( "Dobra pripravljenost" );
  
  /****************************************
  * Result
  *****************************************/
  
  
  var tickMark = svg.append("g");
  
  tickMark.append("line")
    .attr("x1", resultPos)
  	.attr("y1", chart_y_pos )
    .attr("x2", resultPos )
  	.attr("y2", gauge_h + chart_y_pos )
  	.attr("stroke-width", 3)
  	.attr("stroke", "black");
  
  
  tickMark.append("circle")
    .attr("cx", resultPos)
    .attr("cy", (gauge_h + chart_y_pos) / 2 )
  	.attr("r", 10);

}

function wikiA(){
  var urlA;
  var naslov;

 if(bmi<18.5){
     urlA="https://en.wikipedia.org/api/rest_v1/page/summary/Malnutrition?redirect=false";
     naslov="Podhranjenost";
 }
 else if(bmi>=25 && bmi<30){
   urlA="https://en.wikipedia.org/api/rest_v1/page/summary/Overweight?redirect=false";
   naslov="Čezmerna teža";
 }
 else if(bmi>=30){
   urlA="https://en.wikipedia.org/api/rest_v1/page/summary/Obese?redirect=false";
   naslov="Debelost";
 }
 else{
   urlA="https://en.wikipedia.org/api/rest_v1/page/summary/Human_body_weight?redirect=false";
   naslov="Imate idealno telesno težo";
 }

  var xobj = new XMLHttpRequest();

  xobj.overrideMimeType("application/json");
  xobj.open("GET", urlA, true);
  
  xobj.onreadystatechange = function () {
    // rezultat ob uspešno prebrani datoteki
    if (xobj.readyState == 4 && xobj.status == "200") {
        var jsonA = JSON.parse(xobj.responseText);
        console.log(jsonA.extract);
        document.getElementById("wikiA").innerHTML="<h3>"+naslov+"</h3> <p>"+jsonA.extract+ "</p>";
        
      }
    };
   xobj.send(null);
}

function wikiB(){
     var urlB;
     var naslov;
     
 if(tlakS<110 || tlakD<60){
   urlB="https://en.wikipedia.org/api/rest_v1/page/summary/Hypotension?redirect=false";
   naslov="Hipotenzija, prenizek krvni tlak"
 }
 else if(tlakS>=140 || tlakD>=90){
   urlB="https://en.wikipedia.org/api/rest_v1/page/summary/Hypertension?redirect=false";
   naslov="Hipertenzija, previsok krvni tlak"
 }
 else{
   urlB="https://en.wikipedia.org/api/rest_v1/page/summary/Blood_pressure?redirect=false";
   naslov="Imate normalen krvni tlak";
 }
   var xobj = new XMLHttpRequest();

  xobj.overrideMimeType("application/json");
  xobj.open("GET", urlB, true);
  
  xobj.onreadystatechange = function () {
    // rezultat ob uspešno prebrani datoteki
    if (xobj.readyState == 4 && xobj.status == "200") {
        var jsonB = JSON.parse(xobj.responseText);
        console.log(jsonB.extract);
        document.getElementById("wikiB").innerHTML="<h3>"+naslov+"</h3> <p>"+jsonB.extract+ "</p>";
      }
    };
   xobj.send(null);

}